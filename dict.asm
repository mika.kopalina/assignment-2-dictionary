%include "lib.inc"
%define WORD 8;
%define COMP 1

global find_word

section .text

find_word:
    ; Вход:
    ; rdi - указатель на нуль-терминированную строку
    ; rsi - указатель на начало словаря

	test rdi, rdi
	je .end

    .loop:
    	test rsi, rsi
	je .end

    	push rdi
	push rsi
    	lea rsi, [rsi + WORD]  ; WORD - pointer size (quadword)

    	call string_equals
    
	pop rsi
    	pop rdi
    	
	cmp rax, COMP
    	je .found

    	mov rsi, [rsi]

    	jmp .loop
   .found:
    	mov rax, rsi
    	ret
   .end:
    	xor rax, rax
    	ret
    







