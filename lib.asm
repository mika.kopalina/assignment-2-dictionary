global exit
global string_length
global print_string
global errprint_string
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line

section .text

EXIT equ 60
READ equ 0
WRITE equ 1
STDIN equ 0
STDOUT equ 1
STDERR equ 2


; Принимает код возврата и завершает текущий процесс
exit: 
	mov rax, EXIT     ; exit syscall number или номер системного вызова exit
    	syscall         ; системный вызов
    
 
; Принимает указатель на нуль-терминированную строку, возвращает её длину
; нуль-термированнная - та, которая заканчивается на 0x00
; rcx - предназначен для циклов, в данном случае - счётчик
string_length:
    xor rax, rax    ; аннулируем счётчик
.loop:
    cmp byte [ rdi + rax ], 0	; cравнение с 0
    je .end     ; переходим, если сумма равна 0
    inc rax     ; после этой инструкции регистр rax увеличился на 1
    jmp .loop   ; повторяем цикл
.end:    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi - адрес строки - целевой индекс в командах манипулирования строками
; rsi - строка - исходный индекс в командах манипулирования строками
; rdx - длина - сохраняет данные во время операций ввода-вывода
print_string:
    push rdi                ; сохраним caller-saved регистр
    call string_length      ; длина строки передаётся в rax
    mov rdx, rax            ; кладём длину строки в rdx
    pop rsi                 ; кладём адрес начала строки-источника(rdi) в rsi 
    mov rax, WRITE          ; номер системного вызова write
    mov rdi, STDOUT         ; дескриптор stdout
    syscall
    ret


errprint_string:
    	push rdi
    	call string_length ; rax = length of string
    	pop rsi
    	mov rdi, STDERR
    	mov rdx, rax ; rdx = length of string
	mov rax, WRITE
    	syscall
   	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`

; Принимает код символа и выводит его в stdout
    print_char:
        push rdi		; кладем адрес начала символа в стек
        mov rsi, rsp 	; передаем значение указателя стека в rsi - как адрес буфера для вывода
        mov rdx, 1	 	; длина = 1, один символ 
        mov rax, WRITE	; номер системного вызова write
        mov rdi, STDOUT     ; дескриптор stdout
        syscall
        pop rdi	        ; восстанавливаем rdi
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; проверка на знаковое число
    jns print_uint ; переходим, если неотрицательное
    push rdi ; сохраняем регистр rdi
    mov rdi, '-' 
    call print_char ; печатаем знак минуса
    pop rdi     ; восстанавливаем rdi
    neg rdi     ; преобразование в прямой код




; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; rdi - число
print_uint:
    mov rax, rdi	    ; пишем число в rax
    mov r8, 0x0A	    ; делитель 10 записывает в доп регистр
    mov r8, 10
    push 0		    ; в стек отправляем нуль-терминатор
    .loop:		        ; сохраняем каждый остаток в стеке
        xor rdx, rdx	; аннулируем rdx
        div r8		    ; rax - частное, rdx - остаток
        add rdx, 0x30	; ASCII-код числа
        push rdx	    ; сохранение в стек
        cmp rax, r8 	; сравниваем частное с 10
        jae .loop	    ; если больше или равно, то делим повторно
    add rax, 0x30	    ; ASCII-код числа
    cmp rax, 0x30	    ; сравним с 0 
    je .next		    ; если равно-переходим к следующему
    push rax    ; сохранение в стек

    .next:              ; забираем остатки из стека
        pop rdi         ; восстанавливаем rdi
        test rdi, rdi   ; сравнение с нулём
        je .eof         ; если равентсво, то выход из функции
        call print_char
        jmp .next
    .eof:
        ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; rcx - счётчик
string_equals:
    xor rcx, rcx        ; очистка счётчика
 
    .loop:
        mov al, byte [rdi + rcx]	; сохраняем символ первой строки
        cmp al, byte [rsi + rcx]	; сравниваем символы
        jne .no_equals      ; если неравно, то переход на другую часть функции про неравеснтво
        cmp byte [rdi + rcx], 0
	    je .yes_equals      ; если равно, то переход на другую часть функции про равенство
	    inc rcx     ; увеличиваем счётчик
	    jmp .loop
    .no_equals:
        xor rax, rax      ; возвращаем 0
        ret
    .yes_equals:
        mov rax, 1      ; возвращаем 1
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        dec rsp     ; забираем из стека ячейку (ещё можно sub rsp, 1)
        xor rax, rax        ; аннулируем rax 
        mov rdi, STDIN      ; дескриптор stdin             
        mov rsi, rsp    ; записываем адрес символа
        mov rdx, 1      ; записыаем количество символов для чтения              
        syscall
        test rax, rax       ; cmp rax, 0 - сраниваем
        jz .stream_end      ; переход, если ноль
        mov al, [rsp]
	
	.stream_end:
        	inc rsp     ; возращаем ячейку на место - add rsp, 1


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
	mov r13, rsi
    xor r14, r14 ; очистим для подсчета введных символов

    .skip_whitespace:
        call read_char
        cmp rax, 0x9    ;TABULATION_CHAR
        je .skip_whitespace
        cmp rax, 0xA    ;NEWLINE_CHAR
        je .skip_whitespace
        cmp rax, 0x20   ;SPACE_CHAR
        je .skip_whitespace
    .read:
        cmp r14, r13
        je .fail
        cmp al, 0
        je .success
        cmp al, 0x9    ;TABULATION_CHAR
        je .success
        cmp al, 0xA    ;NEWLINE_CHAR
        je .success
        cmp al, 0x20   ;SPACE_CHAR
        je .success

        mov byte [r12 + r14], al
        inc r14
        call read_char
        jmp .read

    .success:
        mov byte [r12 + r14], 0
        mov rax, r12
        mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret

    .fail:
        xor rax, rax
        pop r14
        pop r13
        pop r12
        ret


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер строку из stdin
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
read_line:
	push r12	;сохраняем изначальные значения callee-saved регистров
	push r13
	push r14
	xor r12, r12
	mov r13, rdi
	mov r14, rsi
	dec r14				;уменьшаем размер буффера на 1, чтобы в конце записать в него нуль-терминатор
        .read_line:
		call read_char 		;читаем символ			
                cmp rax, NULL 		;проверка на нуль-терминатор
                jz .line_is_read
		cmp al, `\n`		;проверка на символ переноса строки
                jz .line_is_read
		mov byte[r13 + r12], al ;записываем символ в буффер
		inc r12 		;увеличиваем счетчик прочтенных символов  
                cmp r12, r14 		;сравниваем счетчик, прочтенных символов с размером буффера
                jg .diff_size		;если он больше, то слово слишком большое для буффера
                jmp .read_line
        .line_is_read:			;метка, в которую мы попадем если слово прочитано успешно
		mov byte[r13+r12], NULL 	;записываем нуль-терминатор в конец слова
                mov rax, r13 		;записываем адрес буфера в rax
		mov rdx, r12
                jmp .out		;выходим из функции
        .diff_size:			;метка, в которую мы попадем если слово слишком длинное
                xor rax, rax		;0 в rax
	.out:
		pop r13			;восстанавливаем значения callee-saved регистров
		pop r12
		pop r14
		ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax    ; очистим регистр rax
    xor rdx, rdx      ; сбросим значение длины
    .loop:
        movzx rcx, byte [rdi + rdx]     ; переместить с расширением нуля, то есть заполнить доп биты нулями
        cmp cl, 0     ; сравнение байта с нулем
        je .eof     ; переход к концу, если равно нулю
        sub cl, '0'   ; вычитаем 0 из числа - преобразовываем ASCII в целое число
        cmp cl, 9       ; проверка на цифру, если больше 9, то переходим
        ja .eof

        imul rax, rax, 10   ; умножение результата на 10
        add rax, rcx

        inc rdx     ; переход к следующему символу
        jmp .loop
        
    .eof:              
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'      ; сравнение со знаком минуса
    jne parse_uint
    inc rdi
    call parse_uint
    test rdx, rdx
    jz .fail
    ; учитываем длину знака минуса и превращаем в доп код
    inc rdx
    neg rax
.fail:
    ret



; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi		; вынимаем из стека необходимые данные
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    xor rcx, rcx	; аннулируем rcx
    inc rax		
    cmp rax, rdx
    ja .fail

	.loop:
    	cmp rax, rcx	; сравниваем
	je .end
	mov dl, byte [rdi + rcx]	; загружает байт из памяти по адресу, который вычисляется как сумма значений регистров rdi и rcx, загруженное значение сохраняется в регистр dl 
	mov [rsi + rcx], dl
	inc rcx
    	jmp .loop	
	.end:
    		ret    
	.fail: 
    		xor rax, rax	; аннулируем регистр rax
    		ret
