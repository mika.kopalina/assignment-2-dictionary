%include "lib.inc"
%include "dict.inc"
%include "words.inc"

section .rodata

not_found_word: db "Word not found in dictionary.", 0
key_too_long_msg: db "Entered key too long (should be not more than 255 chars)", 0

section .text
global _start

; Read key from input and try to find it in genders dict.
; Print found gender description or error message.

_start:
    	%define ALLOC_BYTES 256
    	sub rsp, ALLOC_BYTES

    	mov rdi, rsp
    	mov rsi, ALLOC_BYTES ; ограничение на длину на 255 
    	call read_line
    	test rax, rax
    	jz .key_too_long

	; ищем вхождение в словаре    
    	mov rdi, rax
	mov rsi, metka_third	
    	call find_word
	test rax, rax
    	jz .not_found
   
	; выводим значение по ключу в stdout 
        mov r8, rax ; указатель элемента словаря в r8 => вывести значение, соответсвующее введенному ключу
	mov rdi, rsp
	call string_length
	add r8, rax ; смещение указателя с ключа на значение (adding key_length)
	inc r8 ; пропуск нуля-терминатора
	mov rdi, r8 ; указатель значения в rdi
	call print_string
	call print_newline
	jmp .exit

    .not_found:
	mov rdi, not_found_word
	call errprint_string
        jmp .exit

    .key_too_long:
        mov rdi, key_too_long_msg
        call errprint_string

    .exit:
		call exit
        add rsp, ALLOC_BYTES
        ret


