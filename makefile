ASM_FLAGS=-f elf64

%.o: %.nasm
	nasm $(ASM_FLAGS) $< -o $@

dict.o: dict.nasm lib.inc colon.inc

main.o: main.nasm lib.inc dict.inc word.inc

program: main.o lib.o dict.o
	ld -o $@ $^

test:
	@$python3 test3.py

