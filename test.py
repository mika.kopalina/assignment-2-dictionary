import subprocess

# Определение тестовых случаев
test_cases = [
    {"input": "slovo\n", "output": "Hello\n", "error": "\n"},
    {"input": "bykva\n", "output": "abc\n", "error": "\n"},
    {"input": "zvuk\n", "output": "auauau\n", "error": "\n"},
    {"input": "meow\n", "output": "", "error": "Word not found in dictionary.\n"},
    {"input": "murmur\n", "output": "", "error": "Entered key too long (should be not more than 255 chars)\n"},
]

# Инициализация переменных для отслеживания результатов тестов
success_count = 0
failure_count = 0

# Запуск тестов
for index, test_case in enumerate(test_cases):
    p = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate(input=test_case["input"].encode())
    out = out.decode()
    err = err.decode()

    # Проверка соответствия вывода и ошибок ожидаемым значениям
    if out == test_case["output"] and err == test_case["error"]:
        print(f"Тест {index + 1}: .")
        success_count += 1
    else:
        print(f"Тест {index + 1}: F")
        failure_count += 1
        print(f"Ожидаемый вывод: {test_case['output']}")
        print(f"Фактический вывод: {out}")
        print(f"Ожидаемая ошибка: {test_case['error']}")
        print(f"Фактическая ошибка: {err}")
    print("----------------------------")

# Вывод итогов
print("----------------------------")
print(f"Всего тестов: {len(test_cases)}")
print(f"Тестов пройдено: {success_count}")
print(f"Тестов не пройдено: {failure_count}")

# Завершение с соответствующим кодом состояния
if failure_count == 0:
    exit(0)
else:
    exit(1)

